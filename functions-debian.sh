#!/bin/bash

# shellcheck source=functions-common.sh
. "$(dirname "$(readlink -f "$0")")"/functions-common.sh

detect_project() {
  dists=( "$(grep -E ^Source: ./debian/control ./*/debian/control 2>/dev/null | awk '{print $2}' | sort -u)" )

  if [ "${#dists[@]}" -eq 0 ]; then
    echo "No Debian control files found: ./debian/control ./*/debian/control" >&2
    exit 1
  elif [ "${#dists[@]}" -gt 1 ]; then
    echo "More than one Source names found in Debian control files!" >&2
    exit 1
  else
    echo "${dists[0]}"
  fi
}

detect_os() {
  os=$(awk -F"=" '/^ID=/ {print $2}' /etc/os-release)

  if [ -n "$os" ]; then
    echo "$os"
  else
    echo "Could not detect os (ID) from /etc/os-release" >&2
    exit 1
  fi
}

detect_dist() {
  if [ -f /etc/lsb-release ]; then
    dist=$(awk -F= '/^DISTRIB_CODENAME=/ {print $2}' /etc/lsb-release)

    if [ -z "$dist" ]; then
      echo "Could not detect dist (DISTRIB_CODENAME) from /etc/lsb-release" >&2
      exit 1
    fi

    echo "$dist"
  elif grep -q VERSION /etc/os-release; then
    dist=$(awk -F"[)(]+" '/^VERSION=/ {print $2}' /etc/os-release)

    if [ -z "$dist" ]; then
      echo "Could not detect dist (VERSION name) from /etc/os-release" >&2
      exit 1
    fi

    echo "$dist"
  else # try to detect from apt-cache policy
    base_files_policy="$(apt-cache policy base-files | grep -P '^\s*500')"
    if [ -z "$base_files_policy" ]; then
      echo "Could not detect policy repo for base-files!" >&2
      exit 1
    fi

    base_files_suite="$(awk '{ print $3 }' <<<"$base_files_policy")"
    if [ -z "$base_files_suite" ]; then
      echo "Could not detect repo suite for base-files!" >&2
      exit 1
    fi

    dist="$(cut -d/ -f1 <<<"$base_files_suite")"
    if [ -z "$dist" ]; then
      echo "Could not detect dist from policy: $base_files_policy" >&2
      exit 1
    fi

    echo "$dist"
  fi
}

detect_arch() {
  arch=$(dpkg --print-architecture)

  if [ -n "$arch" ]; then
    echo "$arch"
  else
    echo "Could not detect arch from dpkg" >&2
    exit 1
  fi
}

require_var() {
  err=0
  for var in "$@"; do
    if [ -z "${!var}" ]; then
      echo "Variable $var is not set!" >&2
      err+=1
    fi
  done
  [ "$err" -eq 0 ] || exit 1
  echo
}

: "${ICINGA_BUILD_PROJECT:="$(detect_project)"}"
: "${ICINGA_BUILD_OS:="$(detect_os)"}"
: "${ICINGA_BUILD_DIST:="$(detect_dist)"}"
: "${ICINGA_BUILD_ARCH:="$(detect_arch)"}"
: "${ICINGA_BUILD_TYPE:="release"}"
: "${ICINGA_BUILD_UPSTREAM_BRANCH:="master"}"
: "${ICINGA_BUILD_DEB_FLAVOR:="$ICINGA_BUILD_DIST"}"
: "${ICINGA_BUILD_DEB_DEFAULT_ARCH:="amd64"}"
: "${ICINGA_BUILD_IGNORE_LINT:=1}"
: "${ICINGA_BUILD_BRANDING_TAG:=icinga}"
: "${ICINGA_BUILD_BRANDING_VENDOR:=Icinga.com}"
: "${ICINGA_BUILD_BRANDING_VENDOR_EMAIL:=info@icinga.com}"

print_build_env

require_var ICINGA_BUILD_PROJECT ICINGA_BUILD_OS ICINGA_BUILD_DIST ICINGA_BUILD_ARCH ICINGA_BUILD_DEB_FLAVOR ICINGA_BUILD_TYPE
export_build_env

export LANG=C
WORKDIR="$(pwd)"
BUILDDIR='build'
export WORKDIR BUILDDIR

# vi: ts=2 sw=2 expandtab
