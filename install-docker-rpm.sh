#!/bin/bash
#
# This script helps us to prepare a Docker host for the build system
#
# It is used with Docker Machine to install Docker, plus addons
#
# See --engine-install-url at docker-machine create --help


run() {
  (set -x; "$@")
}

echo "Activating RHEL 9 subscription"
sudo subscription-manager register --name=icinga-gitlab-runner-el9 --org=7980812 --activationkey=GitLab_Chahng0wah
sudo subscription-manager repos --enable "codeready-builder-for-rhel-9-x86_64-rpms"
sudo dnf clean all

echo "Installing Docker from CentOS"
sudo mkdir -p /etc/docker
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

echo "Installing QEMU and helpers"
sudo dnf update
sudo dnf install -y qemu-kvm

